import Layout from '../components/layout'
import MultiStepForm from "../components/MultiStepForm";

export default function Home() {
    return (
        <>
            <MultiStepForm/>
        </>
    )
}

Home.getLayout = function getLayout(page) {
    return (
        <Layout>
            {page}
        </Layout>
    )
}
