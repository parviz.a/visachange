import axios from 'axios';
import { BASE_URL } from './Variables'

export const ALL = {
    getCountries() {
        const url = `${BASE_URL}/countries`;
        return axios.get(url);
    },
    getResults(inid) {
        const url = `${BASE_URL}/results?inid=${inid}`;
        return axios.get(url);
    },
    getVisaTypes() {
        const url = `${BASE_URL}/visatypes`;
        return axios.get(url);
    },
    getQuestions(id){
        const url = `${BASE_URL}/question?inid=${id}`; //question?inid=3c0d4111-6f26-4dc6-8e69-c562d63780f9
        return axios.get(url);
    },

    startInterview(data) {
        const url = `${BASE_URL}/start`;
        return axios.post(url, data);
    },

    questionInterview(inid, data) {
        const url = `${BASE_URL}/question?inid=${inid}`;
        return axios.post(url, data);
    },
}
