import axios from 'axios';
import { ALL } from './All'

export default {
    ...ALL,
}

axios.interceptors.request.use(
    config => {
        config.headers = Object.assign({}, config.headers);
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use((response) => response, (error) => {
    throw error;
});
