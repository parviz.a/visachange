import styles from "../styles/Home.module.css";

export default function Layout({children}) {
    return (
        <div>
            <section className={styles.showcase}>
                <header className={styles.header}>
                    <h2 className={styles.logo}>PICKVISA</h2>
                    <div className={styles.toggle}></div>
                </header>

                <div className={styles.overlay}></div>
                {children}
            </section>
        </div>
    )
}