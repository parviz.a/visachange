import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Skeleton from "@material-ui/lab/Skeleton";
import Grid from "@material-ui/core/Grid";

function TypographyDemo(props) {
    const { loading = false } = props;

    return (
        <div>
            <Typography component="div" variant="h3">
                {loading ? <Skeleton /> : ""}
            </Typography>
        </div>
    );
}

TypographyDemo.propTypes = {
    loading: PropTypes.bool
};

export default function FormSkeletonTypography() {
    return (
        <Grid container spacing={3}>
            <Grid item xs={3}>
                <TypographyDemo loading />
            </Grid>
            <Grid item xs={3}>
                <TypographyDemo loading />
            </Grid>
            <Grid item xs={3}>
                <TypographyDemo loading />
            </Grid>
        </Grid>
    );
}
