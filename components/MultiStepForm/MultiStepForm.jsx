import React, { useState } from "react";
import { Provider } from "./MultiStepContext";
import Travel from "./Travel";
import User from "./User";
import Quiz from "./Quiz";
import styles from "../../styles/Home.module.css";
import Review from "./Review";

const travelInitialState = {
    country_to: "",
    visa_type: null,
};

const userInitialState = {
    country_from: "",
    resident_of: "",
    source:"visachance.com",
    name: ""
};

const renderStep = (step) => {
    switch (step) {
        case 0:
            return <Travel />;
        case 1:
            return <User />;
        case 2:
            return <Quiz />;
        case 3:
            return <Review />
        default:
            return null;
    }
};

const MultiStepForm = () => {
    const [travel, setTravel] = useState(travelInitialState);
    const [user, setUser] = useState(userInitialState);
    const [inid, setInid] = useState(userInitialState);

    const [currentStep, setCurrentStep] = useState(0);

    const next = () => {
        if (currentStep === 3) {
            setCurrentStep(0);
            setTravel(travelInitialState);
            setUser(userInitialState);
            return;
        }
        setCurrentStep(currentStep + 1);
    };
    const prev = () => setCurrentStep(currentStep - 1);
    return (
        <Provider value={
            {
                inid,
                setInid,
                travel,
                setTravel,
                next,
                prev,
                user,
                setUser
            }
        }>
            <div className={styles.text}>
                <div className={styles.search}>
                    {renderStep(currentStep)}
                </div>
            </div>
        </Provider>
    );
};
export default MultiStepForm;
