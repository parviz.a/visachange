import React, {useState, useEffect, useContext } from "react";
import MultiStepFormContext from "./MultiStepContext";
import {Formik, Form, Field} from 'formik';
import {Select, FormControl, Button, Grid, MenuItem, InputLabel} from '@material-ui/core';
import apiService from "../../pages/services/ApiService";
import styles from "../../styles/Home.module.css";
import FormSkeletonTypography from "../FormSkeleton";
const Travel = () => {
    const { travel, setTravel, next, prev } = useContext(MultiStepFormContext);
    const [loading, setLoading] = useState(false);
    const [countries, setCountries] = useState([])
    const [visaTypes, setVisaTypes] = useState([])

    function handleSubmit(values) {
        setTravel(values);
        next();
    };

    function fetchCountries() {

        apiService.getCountries()
            .then( async  res => {
                if(res.status == 200){
                    await setCountries(res.data.data);
                }
            })
            .catch(e => {
                console.log(e);
            }).finally(() => {
        })
    }

    function fetchVisaTypes() {
        apiService.getVisaTypes()
            .then(async res => {
                if(res.status == 200){
                    await setVisaTypes(res.data);
                }
            }).catch(e => {
            console.log(e);
            })
            .finally(() => {
                setLoading(false)
            })
    }


    useEffect(() => {
        setLoading(true);
        fetchCountries();
        fetchVisaTypes();
    }, []);

    if(loading) return (
        <FormSkeletonTypography/>
    )
    return (
        <Formik
            initialValues={travel}
            onSubmit={(values) => {
                setTravel(values);
                next();
            }}
            validate={(values) => {
                const errors = {};
                if (!values.country_to) errors.country_to = "required";
                if (!values.visa_type) errors.visa_type = "required";
                return errors;
            }}
        >
            {({ handleSubmit,handleChange, errors }) => {
                return (
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <FormControl variant="outlined" fullWidth>
                                <InputLabel id="country-label">Country</InputLabel>
                                <Select
                                    labelId="country-label"
                                    id="country_to"
                                    name={"country_to"}
                                    onChange={handleChange}
                                    label="Country"
                                    defaultValue=""
                                >
                                    <MenuItem value="">
                                        <em>Choose a country</em>
                                    </MenuItem>

                                    {
                                        countries.map( (c, index) => {
                                            return (<MenuItem value={c.short_code} key={index}>{c.name}</MenuItem>);
                                        })
                                    }
                                </Select>
                            </FormControl>
                        </Grid>

                        <Grid item xs={4}>
                            <FormControl variant="outlined" fullWidth>
                                <InputLabel id="visa-type-label">Visa type</InputLabel>
                                <Select
                                    labelId="visa-type-label"
                                    id="visa_type"
                                    name={"visa_type"}
                                    label="Visa Type"
                                    onChange={handleChange}
                                    defaultValue=""
                                >
                                    <MenuItem value="">
                                        <em>Choose a visa type</em>
                                    </MenuItem>

                                    {
                                        visaTypes.map( v => {
                                            return (<MenuItem value={v.id} key={v.id}>{v.title}</MenuItem>);
                                        })
                                    }
                                </Select>
                            </FormControl>
                        </Grid>

                        <Grid item xs={4}>
                            <Button variant="contained" color="primary" fullWidth className={styles.nextBtn} onClick={handleSubmit}>
                                Next
                            </Button>
                        </Grid>
                    </Grid>
                );
            }}
        </Formik>
    );
};
export default Travel;
