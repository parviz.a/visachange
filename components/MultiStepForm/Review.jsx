import React, { useContext, useEffect, useState } from "react";
import MultiStepFormContext from "./MultiStepContext";
import apiService from "../../pages/services/ApiService";
import { Button, Grid } from "@material-ui/core";
import styles from '../../styles/Quiz.module.css';

const Review = () => {
    const { inid, next } = useContext(MultiStepFormContext);
    const [ result, setResult ] = useState({});
    const [ decline, setDecline] = useState(false);

    function getResult(id) {
        apiService.getResults(id)
          .then(res => {
            if(res.status){
              setResult(res.data.data);
            }
          }).catch(e => {
            setDecline(true);
        })
    }

    useEffect(() => {
        getResult(inid);
    }, [])

    function handleClick() {
      next();
    }

    if(decline) { return (
      <div>
        <h1>Your results:</h1>
        <p>No result...</p>
        <Button variant="contained" color="primary" onClick={handleClick}>
          Complete
        </Button>
      </div>
    );
    }
    return (
        <div class={styles.result}>
            <h1>Your results:</h1>
            <p>Visa status: {result.visa_status}</p>
            <p>Point: {result.total_result}</p>
            <Button variant="contained" color="primary" onClick={handleClick}>
                Complete
            </Button>
        </div>
    );
};
export default Review;
