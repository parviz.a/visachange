import React, {useState, useEffect, useContext } from "react";
import MultiStepFormContext from "./MultiStepContext";
import {Formik, Form, Field} from 'formik';
import {Select, FormControl, Button, Grid, MenuItem, InputLabel,TextField} from '@material-ui/core';
import apiService from "../../pages/services/ApiService";
import FormSkeletonTypography from "../FormSkeleton";
import styles from "../../styles/Home.module.css";

const User = () => {
    const { user, setUser, next } = useContext(MultiStepFormContext);
    const [loading, setLoading] = useState(false);
    const [countries, setCountries] = useState([])

    function fetchCountries() {
        setLoading(true);
        apiService.getCountries()
            .then( res => {
                if(res.status == 200){
                    setCountries(res.data.data);
                }
            })
            .catch(e => {
                console.log(e);
            }).finally(() => {
                setLoading(false);
        })
    }

    useEffect(() => {
        fetchCountries();
    }, []);

    if(loading) return ( <FormSkeletonTypography/> );

    return (
        <Formik
            initialValues={user}
            onSubmit={(values) => {
                setUser(values);
                next();
            }}
            validate={(values) => {
                const errors = {};
                if (!values.country_from) errors.country_from = "required";
                if (!values.resident_of) errors.resident_of = "required";
                if (!values.source) errors.source = "required";
                if (!values.name) errors.name = "required";
                return errors;
            }}
        >
            {({ handleSubmit, handleChange, errors }) => {
                return (
                    <Grid container spacing={4}>
                        <Grid item xs={3}>
                            <FormControl variant="outlined" fullWidth>
                                <InputLabel id="country-label">Country of citizenship</InputLabel>
                                <Select
                                    labelId="country-label"
                                    id="country_from"
                                    name={"country_from"}
                                    onChange={handleChange}
                                    label="Country of citizenship"
                                    defaultValue=""
                                >
                                    <MenuItem value="">
                                        <em>Choose a country</em>
                                    </MenuItem>

                                    {
                                        countries.map( (c, index) => {
                                            return (<MenuItem value={c.short_code} key={index}>{c.name}</MenuItem>);
                                        })
                                    }
                                </Select>
                            </FormControl>
                        </Grid>

                        <Grid item xs={3}>
                            <FormControl variant="outlined" fullWidth>
                                <InputLabel id="visa-type-label">Country of residence</InputLabel>
                                <Select
                                    labelId="visa-type-label"
                                    id="resident_of"
                                    name={"resident_of"}
                                    label="Country of residence"
                                    onChange={handleChange}
                                    defaultValue=""
                                >
                                    <MenuItem value="">
                                        <em>Choose a country</em>
                                    </MenuItem>

                                    {
                                        countries.map( (c,index) => {
                                            return (<MenuItem value={c.short_code} key={index}>{c.name}</MenuItem>);
                                        })
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField id="outlined-basic" label="Name" variant="outlined" name={"name"} onChange={handleChange} fullWidth/>
                        </Grid>

                        <Grid item xs={3}>
                            <Button variant="contained" color="primary" fullWidth className={styles.nextBtn} onClick={handleSubmit}>
                                NEXT
                            </Button>
                        </Grid>
                    </Grid>
                );
            }}
        </Formik>
    );
};
export default User;
