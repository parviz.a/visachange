import React, {useContext, useEffect, useState} from "react";
import styles from "../../styles/Quiz.module.css";
import {Typography} from '@material-ui/core';
import apiService from "../../pages/services/ApiService";
import MultiStepFormContext from "./MultiStepContext";
import SkeletonTypography from "../Skeleton";

const Quiz = () => {
    const { travel,setInid, user, next } = useContext(MultiStepFormContext);
    const [survey, setSurvey] = useState("");
    const [question, setQuestion] = useState({answers: []})
    const [loading, setLoading] = useState(false);

    function start(){
        setLoading(true)

        let form = {
            ...user,
            ...travel,
        }

        apiService.startInterview(form)
            .then(res => {
                if(res.status){
                    setSurvey(res.data.data.inid);
                    setInid(res.data.data.inid);
                    getQuestion(res.data.data.inid);
                }
            })
    }

    function getQuestion(id) {
        setLoading(true)
        apiService.getQuestions(id)
            .then(res => {
                console.log(res);
                if(res.status){
                    if(!res.data.data) {
                        next();
                    }else{
                        setQuestion(res.data.data);
                    }
                }
            }).catch(e => {
                next();
        })
          .finally(() => {
                setLoading(false)
        })
    }

    useEffect( () =>{
        start();
    },[]);

    function handleSubmit(id) {
        let data = {
            choice: id,
        }
        apiService.questionInterview(survey, data)
            .then(res => {
                console.log(res);
                if(res.status){
                    getQuestion(survey);
                }
            })
    }

    if(loading) return (
        <SkeletonTypography/>
    );

    return (
        <>
            <div className={styles.quizContainer} id="quiz">
                <div className={styles.quizHeader}>
                    <Typography variant="h4" gutterBottom>
                        {question.title}
                    </Typography>
                    <ul className={styles.list}>
                        {question.answers.map( a => {
                            return(
                                <li className={styles.list__item} key={a.id} onClick={() => handleSubmit(a.id)}>{a.title}</li>
                            )
                        })}
                    </ul>
                </div>
            </div>
        </>
    );
};
export default Quiz;